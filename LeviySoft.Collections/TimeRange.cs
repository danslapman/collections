using System;

namespace LeviySoft.Collections
{
    public sealed class TimeRange
    {
        public DateTime StartDate { get; private set; }
        public DateTime EndDate { get; private set; }
        public DateTime OriginalStartDate { get; private set; }
        public DateTime OriginalEndDate { get; private set; }

        public TimeRange(DateTime date): this(date, date) { }

        public TimeRange(DateTime startDate, DateTime endDate)
        {
            OriginalStartDate = startDate;
            OriginalEndDate = endDate;

            startDate = startDate.Date;
            endDate = endDate.Date;
            if (startDate > endDate)
            {
                var temp = startDate;
                startDate = endDate;
                endDate = temp;
            }
            endDate = endDate.AddDays(1);
            StartDate = startDate;
            EndDate = endDate;
        }
    }
}
